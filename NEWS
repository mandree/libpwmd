libPWMD v8.3.2
--------------
Now appends the current git commit hash to the version string to aid in
development.

Add pwmc command line option --socket to replace --url.

A few bug fixes. See ChangeLog for details.

Releases are now signed using a new signing key. The fingerprint of the new
key is 6078FEB430EFA427499E6E78555B69666326961C and is cross-signed with my
new primary key which is cross-signed with my old primary key. I don't believe
the old keys to be compromised; it is only to rotate them and update to newer
standards.


libPWMD v8.3.1
--------------
Updated the tutorial.

Fixed 'make deb' when run from a tarball dist.

Fixed win32 getting stuck in getaddrinfo(). Libpwmd no longer spawns a thread
for this platform when doing a DNS lookup.


libPWMD v8.3.0
--------------
Now requires pwmd-3.2.0 or later.

Added BULK protocol command function helpers pwmd_bulk(), pwmd_bulk_append(),
pwmd_bulk_append_rc(), pwmd_bulk_finalize() and pwmd_bulk_result().

Added options PWMD_OPTION_LOCK_TIMEOUT and PWMD_OPTION_STATE_STATUS to lessen
socket IO upon connecting.


libPWMD v8.2.2
--------------
Support pwmd-3.1.1 XFER status messages.

Passphrase quality checking (--enable-quality) is enabled by default during
configure.

Build and bug fixes. See ChangeLog for details.


libPWMD v8.2.1
--------------
The project has moved to GitLab (https://gitlab.com/bjk/libpwmd/wikis).
Downloads are still available at SourceForge but the issue tracker, git
repository and wiki are now hosted at GitLab.

Append libassuan and pthread linker flags to pkg-config.

Ported to win32.

Fixed pwmc '.listkeys' time output.


libPWMD v8.2.0
--------------
Added pwmd_command_fd() to associate an existing file descriptor with a pwm_t
handle.

Updated the zxcvbn-c passphrase quality checking library.


libPWMD v8.1.0
--------------
Added pwmd_genkey() to cope with pinentry and the new pwmd GENKEY command.

Added output of the keygrip to the pwmc .listkeys command.


libPWMD v8.0.0
--------------
Pwmc command line options have changed to reflect changes in made to pwmd-3.1.

Added PWMD_OPTION_SERVER_VERSION which is set after the connection completes.

Add pwmd_cancel() to cancel a connection. Unable cancel a command, yet.

Removed references to gnutls_global_set_mem_functions(). These are deprecated
and no longer do anything anymore.

Added support for passphrase protected private SSH keys. This adds
PWMD_OPTION_SSH_NEEDS_PASSPHRASE which will prompt for the passphrase needed
to open the private SSH key during pwmd_connect() or obtain it with
PWMD_OPTION_SSH_PASSPHRASE. This feature requires that libssh2 has been built
with OpenSSL.

PWMD_OPTION_KNOWNHOST_CB uses a default when not set.

Added pwmd_gnutls_error() to replace pwmd_tls_error() to fetch both the GnuTLS
error code and error string.

Replaced cracklib passphrase quality checking with zxcvbn-c.

Added pwmd_test_quality() to return the entropy of a passphrase in bits.

Added PWMD_OPTION_TLS_PRIORITY. This changes pwmd_connect() parameters.

Read libpwmd settings from ~/.config/libpwmd.conf. This removes
~/.pwmd/pinentry.conf support. See docs for details.

Renamed pwmc --tls-verify to --no-tls-verify since hostname checking is done
by default now.

The DISPLAY and TERM environment variables are unset when empty and set with
pwmd_setopt().

Now supports TLS rehandshaking.


libPWMD v7.2.0
--------------
Require an SHA-256 TLS server fingerprint rather than SHA-1.

Added pwmd_tls_error() to get the return code of a failed gnutls function.
This error can be described with gnutls_strerror().

Added PWMD_OPTION_SIGPIPE to prevent ignoring SIGPIPE which is now the
default.

Fixed TLS wait interval when EAGAIN occured.

A couple other TLS bug fixes. See ChangeLog for details.


libPWMD v7.1.1
--------------
Add (mostly useless) pwmc interactive option ".set pinentry-timeout". This
really only makes the local pinentry thread-safe.


libPWMD v7.1.0
--------------
Added pwmd_passwd() to change the passphrase of a data file.

pwmc: add the .passwd dot command.

Added pwmd_getopt() to retrieve the value of a pwmd_option_t.


libPWMD v7.0.1
--------------
Add pwmd_set_pointer() and pwmd_get_pointer().

Add 'configure --with-pwmd-socket=path' to specify the default (local)
socket to connect to.


libPWMD v7.0.0
--------------
This is a major version and some function names and their parameters
have changed while others have been removed. Please read the
libpwmd(3) manual page for complete API documentation and pwmc(1) for
command line client usage.

Removed libpth2 support.

Removed non-blocking API functions. Applications should use threads
instead.

Reimplemented remote TLS connection support.

pwmc: Interactive mode is enabled by default when input is from a
terminal.

Gpg-error codes are now returned along with the error source.
