libpwmd supports remote access to a pwmd server via TLS and by using the
libssh2 library.  The pwmd_connect() function is used to connect to the remote
socket just as the local socket by prefixing the socket with ssh:// followed
by the hostname.  After resolving the hostname and connecting, an ssh session
is created and the hostkey is verified against a file containing known hosts.
A passphrase protected private SSH key is supported but only when libssh2 has
been built using OpenSSL.

After verification succeeds, a channel is opened which spawns a shell. This
shell should execute a proxy to the pwmd server by connecting to the local
socket. I use 'socat', it's really handy.

In order to get this to work you need to put the following in your
~/.ssh/authorized_keys file on the machine running the ssh and pwmd server. It
should be prepended to the public key portion of the private key that your
libpwmd client will use:

    command="socat UNIX-CONNECT:$HOME/.pwmd/socket -" ... identity.pub ...

Now when libpwmd spawns the SSH shell, 'socat' will read the stdout of the SSH
shell and redirect it to the pwmd socket. The output of pwmd works the same:
redirecting pwmd's socket output to the stdin of the SSH shell and back to the
libpwmd client.

You can use pwmc to try it out:
    # Generate an SSH key that the client will use. Then copy the contents of
    # the generated public key to the SSH servers authorized_keys file and
    # prepend the above mentioned line to it. You will need both the generated
    # public and private keys when connecting to the server.
    ssh-keygen -f keyfile

    # If you have an ssh agent running, you can also do the following:
    ssh-add keyfile

    # List the contents of the pwmd 'datafile' on the remote SSH server by
    # connecting as the specified 'user'. If you have an ssh agent running and
    # did the above command, you can omit the --identity option.
    echo list | pwmc --url ssh://user@hostname --identity keyfile datafile


Ben Kibbey <bjk@luxsci.net>
http://pwmd.sourceforge.net/
