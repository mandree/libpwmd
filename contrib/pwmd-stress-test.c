/*
    Copyright (C) 2016-2018 Ben Kibbey <bjk@luxsci.net>

    This file is part of libpwmd.

    Libpwmd is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    Libpwmd is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Libpwmd.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <libpwmd.h>
#include <pthread.h>
#include <string.h>

static char *socket, *file;
static int remaining;
static pthread_mutex_t count_mutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t cmd_mutex = PTHREAD_MUTEX_INITIALIZER;
static int lock;
static int states;
static int lock_timeout;
static int quiet;
static char **cmds;
static int bulk;
static int cmd_count;

static gpg_error_t
status_cb (void *data, const char *line)
{
  pthread_t tid = pthread_self ();

  if (quiet < 1)
    fprintf(stdout, "STATUS: %p: %s\n", (pthread_t *)tid, line);
  return 0;
}

void *
start (void *data)
{
  pwm_t *pwm = NULL;
  gpg_error_t rc;
  pthread_t tid = pthread_self ();
  char *result;
  char **p;
  int c;

  if (quiet < 2)
    fprintf(stdout, "START: %p\n", (pthread_t *)tid);
  rc = pwmd_new ("test-client", &pwm);
  if (rc)
    goto done;

  pwmd_setopt (pwm, PWMD_OPTION_LOCK_ON_OPEN, lock);
  pwmd_setopt (pwm, PWMD_OPTION_STATUS_CB, status_cb);

  rc = pwmd_connect (pwm, socket, NULL);
  if (rc)
    goto done;

  if (lock_timeout)
    {
      rc = pwmd_command (pwm, NULL, NULL, NULL, NULL, "OPTION lock-timeout=%i",
                         lock_timeout);
      if (rc)
        goto done;
    }

  if (states)
    {
      rc = pwmd_command (pwm, NULL, NULL, NULL, NULL, "OPTION client-state=%i",
                         states);
      if (rc)
        goto done;
    }

  rc = pwmd_process (pwm);
  if (rc)
    goto done;

  if (file)
    {
      rc = pwmd_open (pwm, file, NULL, NULL);
      if (rc)
        goto done;
    }

  rc = pwmd_process (pwm);
  if (rc)
    goto done;

  if (bulk)
    {
      char *tcmds = NULL;
      int n = 0;

      for (c = 0; c < cmd_count; c++)
        {
          for (p = cmds; p && *p; p++, n++)
            {
              char buf[16];
              char *args = NULL;
              size_t len = 0;
              char *s;

              pthread_mutex_lock (&cmd_mutex);
              s = strchr (*p, ' ');
              if (s)
                {
                  char *t = *p;

                  len = strlen (*p) - strlen (s+1)-1;
                  t[len] = 0;
                  args = t+len+1;
                }

              snprintf (buf, sizeof (buf), "%i", n+c);
              rc = pwmd_bulk_append (&tcmds, buf, strlen (buf), *p, args,
                                     args ? strlen (args) : 0, NULL);
              if (len)
                {
                  char *t = *p;
                  t[len] = ' ';
                }

              pthread_mutex_unlock (&cmd_mutex);

              if (rc)
                {
                  pwmd_free (tcmds);
                  goto done;
                }
            }
        }

      rc = pwmd_bulk_finalize (&tcmds);
      if (!rc)
        rc = pwmd_bulk (pwm, &result, NULL, NULL, NULL, tcmds, strlen (tcmds));
      if (rc)
        {
          fprintf(stderr, "\n%u '%s'\n", rc,tcmds);
        goto done;
        }

      pwmd_free (tcmds);
      if (quiet < 1)
        fprintf(stdout, "RESULT: %p: '%s'\n", (pthread_t *) tid, result);

      pwmd_free (result);
      rc = pwmd_process (pwm);
      goto done;
    }

  for (c = 0; c < cmd_count; c++)
    {
      for (p = cmds; p && *p; p++)
        {
          if (quiet < 2)
            fprintf(stdout, "Doing: '%s'\n", *p);

          rc = pwmd_command (pwm, &result, NULL, NULL, NULL, *p);
          if (rc)
            goto done;

          if (quiet < 1)
            fprintf(stdout, "RESULT: %p: '%s'\n", (pthread_t *) tid, result);
          pwmd_free (result);
          rc = pwmd_process (pwm);
        }
    }

done:
  pwmd_close (pwm);

  if (rc && quiet < 3)
    fprintf(stdout, "ERROR: %p: %u: %s\n", (pthread_t *)tid, rc,
            gpg_strerror(rc));

  pthread_mutex_lock (&count_mutex);
  remaining--;
  pthread_mutex_unlock (&count_mutex);
  if (quiet < 2)
    fprintf(stdout, "END: %p\n", (pthread_t *)tid);
  pthread_exit (NULL);
  return NULL;
}

static void
usage (const char *pn, int status)
{
  fprintf(status == EXIT_SUCCESS ? stdout : stderr,
          "Usage: %s [-hsnqb] [-l N] <clients> <socket> [<datafile>] [-c N] -e '<command_with_args>' ...\n", pn);
  fprintf(status == EXIT_SUCCESS ? stdout : stderr,
          "  -n     don't lock data file mutex\n"
          "  -l N   lock timeout in tenths of a second\n"
          "  -q     quiet (can be used more than once)\n"
          "  -s     receive client state status messages\n"
          "  -h     this help text\n"
          "  -c     run the specified command N times per client\n"
          "  -b     use a BULK inquire to run the commands\n"
          "  -e ... run the specified command and arguments (can be used more than once)\n");
  exit (status);
}

int main(int argc, char **argv)
{
  int i;
  pthread_attr_t attr;
  unsigned nclients;
  int opt;
  int total = 0;
  
  lock = 1;
  cmd_count = 1;

  while ((opt = getopt (argc, argv, "hnsl:qe:bc:")) != -1)
    {
      switch (opt)
        {
        case 'c':
          cmd_count = atoi (optarg);
          break;
        case 'b':
          bulk = 1;
          break;
        case 'q':
          quiet++;
          break;
        case 'l':
          lock_timeout = atoi (optarg);
          break;
        case 'n':
          lock = 0;
          break;
        case 's':
          states = 1;
          break;
        case 'h':
          usage (argv[0], EXIT_SUCCESS);
          break;
        case 'e':
          cmds = realloc (cmds, (total+2)*sizeof (char *));
          cmds[total++] = strdup (optarg);
          cmds[total] = NULL;
          break;
        default:
          usage (argv [0], EXIT_FAILURE);
          break;
        }
    }

  if (argc - optind < 2 || !cmds)
    usage (argv[0], EXIT_FAILURE);

  nclients = atoi (argv[optind++]);
  socket = strdup (argv[optind++]);
  file = strdup (argv[optind++]);

  pwmd_init ();
  pthread_attr_init (&attr);
  pthread_attr_setdetachstate (&attr, PTHREAD_CREATE_DETACHED);

  for (i = 0; i < nclients; i++)
    {
      pthread_t tid;
      int e = pthread_create (&tid, NULL, start, NULL);

      if (e)
        {
          if (quiet < 3)
            fprintf(stdout, "%s\n", strerror (e));
          continue;
        }

      pthread_mutex_lock (&count_mutex);
      remaining++;
      pthread_mutex_unlock (&count_mutex);
    }

  pthread_attr_destroy (&attr);

  for (; remaining;)
    {
      int n;

      pthread_mutex_lock (&count_mutex);
      n = remaining;
      pthread_mutex_unlock (&count_mutex);

      if (quiet < 2)
        fprintf(stdout, "%i clients remain ...\n", n);
      usleep (100000);
    }

  pwmd_deinit ();
  exit (EXIT_SUCCESS);
}
