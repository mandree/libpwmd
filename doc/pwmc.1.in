.\" Copyright (C) 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015
.\"               2016
.\" Ben Kibbey <bjk@luxsci.net>
.\" 
.\" This file is part of libpwmd.
.\" 
.\" Libwmd is free software: you can redistribute it and/or modify
.\" it under the terms of the GNU General Public License as published by
.\" the Free Software Foundation, either version 2 of the License, or
.\" (at your option) any later version.
.\" 
.\" Libwmd is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.\" GNU General Public License for more details.
.\" 
.\" You should have received a copy of the GNU General Public License
.\" along with Libpwmd.  If not, see <http://www.gnu.org/licenses/>.
.de URL
\\$2 \(laURL: \\$1 \(ra\\$3
..
.if \n[.g] .mso www.tmac
.TH PWMC 1 "18 Sep 2016" "@VERSION@" "Password Manager Client"
.SH NAME

pwmc \- send a command to a pwmd server
.SH SYNOPSIS
.B pwmc
[options] [file]

.SH DESCRIPTION
.B pwmc
is a
.BR libpwmd (3)
client for
.BR pwmd (1).
Pwmc reads pwmd protocol commands from either standard input or
interactively by using libreadline to prompt for input. The
interactive mode allows for more than one command to be sent to the
pwmd server.

.SH OPTIONS
.TP
.I "\--socket <string>"
A pwmd socket to connect to. The available URL types depend on options
enabled at compile-time. The default when not specified is
.B @pwmd_socket@.
See
.B EXAMPLES
for details.

.TP
.I "\--connect-timeout <seconds>"
The number of seconds before timing out when connecting to a remote
server. The default is 120 seconds.

.TP
.I "\--socket-timeout <seconds>"
The number of seconds before a remote command will timeout. The
default is 300 seconds.

.TP
.I "\--ca-cert, <filename>"
The X509 certificate authority file to use when validating the pwmd
TLS server certificate. Required when connecting via TLS.

.TP
.I "\--client-cert, <filename>"
The X509 client certificate to use when authenticating a TLS
connection. Required when connecting via TLS.

.TP
.I "\--client-key, <filename>"
The X509 key file to use to unlock the client certificate.  Required
when connecting via TLS.

.TP
.I "\--tls-priority, <string>"
A string representing compression, cipher and hashing algorithm priorities for
a TLS connection. See the GnuTLS documentation for details. The default is
.B SECURE256:SECURE192:SECURE128:-VERS-SSL3.0.

.TP
.I "\--no-tls-verify"
Disable verification of the TLS server hostname stored in the server
certificate. The default is enabled.

.TP
.I "\--tls-fingerprint <string>"
Verify the
.BR pwmd
TLS server certificate against the specified SHA-256 hash.

.TP
.I "\--no-ssh-agent"
Normally when the SSH_AUTH_SOCK environment variable is set pwmc will get
authentication details from the ssh agent. When this option is specified or
when the environment variable is not set a private key must be specified on
the command line with the
.I "\-\-identity"
option. See
.BR ssh-add (1)
for how to add a private key to the ssh agent. Note that the
.I "\-\-identity"
command line option has priority over the ssh agent.

.TP
.I "\-\-identity, -i <filename>"
The SSH identity file use when connecting via SSH.

.TP
.I "\-\-knownhosts, -k <filename"
The SSH knownhosts file to use when connection via SSH. The default is
~/.ssh/knownhosts.

.TP
.I "\-\-ssh-needs-passphrase"
This option specifies that the SSH identity file requires a passphrase to
connect. Note that libssh2 needs to be built with OpenSSL to support
decryption of a private SSH key.

.TP
.I "\-\-ssh-passphrase-file <filename>"
Obtain the passphrase to decrypt the SSH private key from the specified
filename.  Note that libssh2 needs to be built with OpenSSL to support
decryption of a private SSH key.

.TP
.I "\--no-lock"
Do not lock the data file upon opening it. Locking the data file
prevents other clients from opening the same file.

.TP
.I "\--lock-timeout <N>"
The time in tenths of a second to wait for another client to release the lock
held on a data file before returning an error. When not specified or 0, pwmc
will wait indefinitely. When -1, an error will be returned immediately. The
default is 50.

.TP
.I "\--name, -n <string>"
Set the client name to the specified string. This string is what is shown in
the
.BR pwmd (1)
log files. The default is "pwmc".

.TP
.I "\--pinentry, <path>"
The full path to the pinentry binary. The default is specified at
compile-time.

.TP
.I "\--local-pinentry"
Force use of the local pinentry for passphrase retrieval. This will
disable pwmd's pinentry and use a pinentry fork(2)'ed from libpwmd.

.TP
.I "\--no-pinentry"
Disable the use of pinentry entirely. Both pwmd and libpwmd's local
pinentry will be disabled. Pwmc will prompt for the passphrase via
stdin.

.TP
.I "\--ttyname, -y <path>"
The full path of the TTY for
.BR pinentry (1)
to prompt on. The default is the current terminal.

.TP
.I "\--ttytype, -t <string>"
The terminal type of the specified TTY that
.BR pinentry (1)
should use. This is required if
.I "\-\-ttyname"
is specified.

.TP
.I "\--display, -d <string>"
The X11 display that
.BR pinentry (1)
should use. Note that remote connections to pwmd do not use pwmd's
pinentry but use a pinentry fork(2)'ed from libpwmd instead. The default
is obtained from the
.B DISPLAY
environment variable.

.TP
.I "\--lc-ctype, <string>"
For
.BR pinentry (1)
localization.

.TP
.I "\--lc-messages, <string>"
For
.BR pinentry (1)
localization.

.TP
.I "\--tries, <n>"
The number of times before failing when an invalid passphrase is entered in
the
.BR pinentry (1)
dialog. The default is 3.

.TP
.I "\-\-timeout, <seconds>"
The number of seconds before
.BR pinentry (1)
will timeout while waiting for a passphrase. The default is 30.

.TP
.I "\--inquire <command>"
Some pwmd protocol commands use what is called an INQUIRE to retrieve
data from the client. This options argument should be the command name
that uses an inquire. The command data will be read from the file
descriptor specified by the
.I "\-\-inquire\-fd"
option or stdin by default.

.TP
.I "\--inquire-line, -L <string>"
The initial line to send during an inquire and before any other data read from
the inquire file descriptor. This option is useful to specify an
element path without needing to modify the remaining inquire data. See
.B EXAMPLES
below.

.TP
.I "\--inquire-fd <fd>"
Read inquire data from the specified file descriptor. For use with the
.I "\-\-inquire"
option. By default, inquire data is read from stdin. Use
.I "\-\-inquire-line"
to send an initial line before the actual data.

.TP
.I "\-\-inquire-file <filename>"
Read inquire data from the specified filename.  For use with the
.I "\-\-inquire"
option. By default, inquire data is read from stdin. Use
.I "\-\-inquire-line"
to send an initial line before the actual data.

.TP
.I "\--output-fd <fd>"
Redirect the output of the pwmd command to the specified file
descriptor. The default is stdout.

.TP
.I "\--save, -S"
After the command has been processed without error, send the SAVE
command to the server to write any changes to disk.

.TP
.I "\--passphrase-file <filename>"
Read the passphrase used to open a data file from the specified filename.

.TP
.I "\--new-passphrase-file <filename>"
Read a passphrase from the specified filename that is to be used for a
new data file or when changing the passphrase for an existing data
file.

.TP
.I "\--sign-passphrase-file <filename>"
Read a passphrase from the specified filename that is to be used for signing
of a symmetrically encrypted data file.

.TP
.I "\-\-key\-params <filename>"
When saving, use the key parameters obtained from
.B filename
rather than the
.B pwmd
defaults. The parameters are in GnuPG XML format.

.TP
.I "\-\-keyid <fingerprint>[,...]"
Encrypt the XML data to the specified recipients. When not specified, the same
recipients for an existing data file will be used. Otherwise, a new key pair
will be generated.

.TP
.I "\-\-sign-keyid <fingerprint>[,...]"
Sign the encrypted XML data using the specified signing key(s).

.TP
.I "\-\-symmetric
Conventionally encrypt a new data file rather than creating a new key pair.
Note that a signer can also be specified.

.TP
.I "\--no-status"
Do not show server status messages. By default, status messages are
written to stderr.

.TP
.I "\--status-fd <FD>
Redirect status messages to the specified file descriptor. The default is
stderr.

.TP
.I "\--status-ignore <string[,...]>
A list of status messages that are ignored or not displayed. This may be
useful in interactive mode to prevent messing up or confusing the input line.
The default is
.B KEEPALIVE,STATE,GPGME,PASSPHRASE_INFO,PASSPHRASE_HINT.

.TP
.I "\--status-state
Enable receiving of client STATE status messages.

.TP
.I "\--quiet"
Do not show informational messages from pwmc. Implies --no-status.

.TP
.I "\--no-interactive"
Disable interactive mode. By default, interactive mode is enabled when input
is from a terminal. Interactive mode allows for sending more than one command
per connection. See
.B INTERACTIVE
for more information.

.TP
.I "\--version"
Version information.
.TP
.I "\--help"
Show available command line options and syntax.


.SH SSH
Pwmc can connect to a remote pwmd server over an SSH connection by
using a proxy program that can read from stdin and write to a UNIX
domain socket and vice-versa.

Most SSH servers allow special options in the
.B authorized_keys
file that do various things. One such option is the
.B "command"
option that will launch a program when an SSH client connects. We can
use this to start a proxy program that connects to the UNIX domain
socket that pwmd listens on by putting the following in
your
.B ~/.ssh/authorized_keys
file on the remote SSH host:

    command="socat gopen:$HOME/.pwmd/socket -" [...public.key...]

Notice the
.B "command="
is prepended to the public key that was
generated using
.BR ssh-keygen (1)
and specified using the
.I "\-\-identity"
command line option.

The
.BR socat (1)
command can be replaced with any utility that can read from stdin and write
to a UNIX domain socket.


.SH PINENTRY
.BR pinentry (1)
is a program that prompts a user for input. This is currently not
supported when connected to a remote pwmd server since X11 port
forwarding has yet to be implemented. If a pinentry is required over a
remote connection then a local pinentry fork'ed from libpwmd will be
used unless the
.I "\-\-no\-pinentry"
command line option was specified.

The terminal, terminal type or
.B DISPLAY
that pinentry will prompt on is either
set with the command line options or uses options set in
.B ~/.config/libpwmd.conf
when available (see below). Otherwise the current terminal and
terminal type or X11
.B DISPLAY 
is used.


.SH CONFIGURATION FILE
The
.B ~/.config/libpwmd.conf
file contains one
.B NAME=VALUE
pair per line. Comments begin with a '#'. Options specified on the command
line override the settings in this file.

.TP
.I pinentry-path = /path
Same as --pinentry.

.TP
.I pinentry-display = string
Same as --display.

.TP
.I pinentry-ttyname = /path
Same as --ttyname.

.TP
.I pinentry-ttytype = string
Same as --ttytype.

.TP
.I pinentry-lc-messages = string
Same as --lc-messages.

.TP
.I pinentry-lc-ctype = string
Same as --lc-ctype.

.TP
.I pinentry-tries = N
Same as --tries.

.TP
.I pinentry-timeout = N
Same as --timeout.

.TP
.I no-pinentry = 0|1
Same as --no-pinentry.

.TP
.I local-pinentry = 0|1
Same as --local-pinentry.

.TP
.I no-ssh-agent = 0|1
Same as --no-ssh-agent.

.TP
.I no-tls-verify = 0|1
Same as --no-tls-verify.

.TP
.I tls-priority = <string>
Same as --tls-priority.

.TP
.I socket-timeout = N
Same as --socket-timeout.

.TP
.I no-lock = 0|1
Same as --no-lock.

.TP
.I include = filename
Path to a filename containing additional
.B libpwmd.conf
configuration settings. May be useful for a file containing pinentry settings
which is updated as needed.

.SH INTERACTIVE
Interactive mode has a few dot commands. Dot commands (an idea borrowed from
the
.BR sqlite3 (1)
command line program), begin with a
.B .
followed by the command name and any arguments. Here are the available
pwmc interactive commands:

.TP
.I .open <filename>
Open the specified
.B filename
while losing any changes to the current filename. Although the
.B OPEN
protocol command can be used to open a file, this dot command is
recommended because it takes care of pinentry settings among other
things.

.TP
.I .read [--prefix <string>] <filename> <command> [args ...]
For use with a
.B command 
that uses a server INQUIRE. The
.I "\-\-prefix"
option behaves like the
.I "\-\-inquire-line"
command line option. It is the initial line to send before any data
read from
.B filename.

.TP
.I .redir <filename> <command>
Redirect the output of
.B command
to
.B filename.

.TP
.I .set help | <name> [<value>] [...]
Set option
.B name
is set to
.B value.
When no
.B value
is specified, the option
.B name
is unset.
.RS

.TP
.I passphrase-file [<filename>]
Obtain the passphrase from
.B filename
for the next command requiring a passphrase. This is equivalent to the 
.I "\-\-passphrase-file"
command line option. This value will be unset after the next protocol
command to prevent misuse.

.TP
.I new-passphrase-file [<filename>]
Obtain the passphrase from
.B filename
for the next command requiring a new passphrase. This is equivalent to the 
.I "\-\-new-passphrase-file"
command line option. This value will be unset after the next protocol
command to prevent misuse.

.TP
.I sign-passphrase-file [<filename>]
Obtain the passphrase from
.B filename
for the next command requiring a passphrase used for signing. This is
equivalent to the 
.I "\-\-sign-passphrase-file"
command line option and is optionally used for signing a symmetrically
encrypted data file. This value will be unset after the next protocol command
to prevent misuse.

.TP
.I pinentry-timeout <seconds>
Set the amount of seconds before the pinentry program will close and return an
error while waiting for user input.
.RE

.TP
.I .save [args]
Save changes of the document to disk. Using this dot command rather than
the pwmd
.B SAVE
protocol command is recommended since it determines whether to use
pinentry or not.

.TP
.I .passwd
Change the passphrase for the secret key of the opened data file. Using this
dot command rather than the pwmd
.B PASSWD
protocol command is recommended since it determines whether to use
pinentry or not. For symmetrically encrypted data files, use the
.B .save
command instead.

.TP
.I .listkeys [--options] [pattern[,...]]
Display human readable output of the LISTKEYS protocol command.

.TP
.I .help
Show available interactive commands.

.SH EXAMPLES
To list all XML root elements of the data file and use
.BR pinentry (1)
to retrieve the passphrase (if required):
.RS
.I echo list | pwmc filename
.RE
.P
To store an element path and save the file afterwards:
.RS
.I echo -ne 'isp\\\\tsmtp\\\\thostname\\\\tsomehost.com' | pwmc --inquire STORE -S filename
.P
And then to get the content:
.P
.I echo -ne 'get isp\\\\tsmtp\\\\thostname' | pwmc filename
.RE
.P
Store a larger file:
.RS
.I echo -ne 'some\\\\telement\\\\tpath\\\\t' | cat - data_file | pwmc --inquire STORE -S filename
.RE
or
.RS
.I pwmc --inquire STORE --inquire-line 'some\\\\telement\\\\tpath\\\\t' -S filename < data_file
.RE
.P
Clear the file cache for a single file:
.RS
.I echo 'clearcache filename' | pwmc
.RE
.P
To list root elements of a data file which is stored on a remote pwmd server
over an SSH connection:
.RS
.I echo list | pwmc --socket ssh://user@hostname -i ~/.ssh/identity filename
.RE
.P
Connect to a remote pwmd server over TLS, interactively:
.RS
.I pwmc --socket tls://hostname --ca-cert cafile.pem --client-cert client.pem --client-key clientkey.pem filename
.RE

.SH NOTES
Be careful of newline characters when storing data. The data is transferred
and stored exactly as the input is read, newlines and all. If you wonder why
your new passphrase for a service doesn't work then a trailing newline
character may be the reason.

.SH FILES
.TP
.B @pwmd_socket@
Default pwmd socket to connect to.
.TP
.B ~/.config/libpwmd.conf
Default settings that
.BR pinentry (1)
will use for the terminal, terminal type or X11 display.
.TP
.B @pinentry@
Default location of the
.BR pinentry (1)
binary.

.SH AUTHOR
Ben Kibbey <bjk@luxsci.net>
.br
.URL "https://gitlab.com/bjk/libpwmd/wikis" "libpwmd Homepage" .

.SH "SEE ALSO"
.BR pwmd (1),
.BR pinentry (1),
.BR ssh-keygen (1),
.BR authorized_keys (5),
.BR socat (1),
.BR libpwmd (3)

And the pwmd texinfo manual for protocol commands and their syntax.
