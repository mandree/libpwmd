/*
    Copyright (C) 2006-2021 Ben Kibbey <bjk@luxsci.net>

    This file is part of libpwmd.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <sys/types.h>
#include <errno.h>
#include <stdlib.h>
#include <limits.h>

#ifdef HAVE_STRING_H
#include <string.h>
#endif

#include "pinentry.h"
#include "misc.h"

#ifdef WITH_QUALITY
#include "zxcvbn.h"
#endif

static gpg_error_t set_pinentry_strings (pwm_t * pwm, pwmd_pinentry_t which);

static gpg_error_t
launch_pinentry (pwm_t * pwm)
{
  gpg_error_t rc;
  assuan_context_t ctx;
  assuan_fd_t child_list[] = { ASSUAN_INVALID_FD };
  const char *argv[12];
  const char **p = argv;
  static struct assuan_malloc_hooks mhooks = {
    pwmd_malloc, pwmd_realloc, pwmd_free
  };

  *p++ = "";
  if (pwm->pinentry_display)
    {
      *p++ = "--display";
      *p++ = pwm->pinentry_display;
    }

  if (pwm->pinentry_tty)
    {
      *p++ = "--ttyname";
      *p++ = pwm->pinentry_tty;
    }

  if (pwm->pinentry_term)
    {
      *p++ = "--ttytype";
      *p++ = pwm->pinentry_term;
    }

  if (pwm->pinentry_lcctype)
    {
      *p++ = "--lc-ctype";
      *p++ = pwm->pinentry_lcctype;
    }

  if (pwm->pinentry_lcmessages)
    {
      *p++ = "--lc-messages";
      *p++ = pwm->pinentry_lcmessages;
    }

  *p = NULL;
  rc = assuan_new_ext (&ctx, GPG_ERR_SOURCE_PINENTRY, &mhooks, NULL, NULL);
  if (rc)
    return rc;

  rc = assuan_pipe_connect (ctx,
			    pwm->pinentry_path ? pwm->pinentry_path
                            : PINENTRY_PATH, argv, child_list, NULL, NULL, 0);
  if (rc)
    {
      assuan_release (ctx);
      return rc;
    }

  pwm->pinentry_pid = assuan_get_pid (ctx);
  pwm->pctx = ctx;
  rc = set_pinentry_strings (pwm, 0);
  if (rc)
    _pinentry_disconnect (pwm);

  return rc;
}

static gpg_error_t
pinentry_command (pwm_t * pwm, char **result, size_t * len, const char *cmd)
{
  if (len)
    *len = 0;

  if (result)
    *result = NULL;

  if (!pwm->pctx)
    {
      gpg_error_t rc = launch_pinentry (pwm);

      if (rc)
	return rc;
    }

  return _assuan_command (pwm, pwm->pctx, result, len, cmd);
}

#ifdef WITH_QUALITY
static gpg_error_t
quality_cb (void *data, const char *line)
{
#define MAX_ENTROPY 80 // in bits
  pwm_t *pwm = data;
  char buf[5];
  double match;

  if (strncmp (line, "QUALITY ", 8))
    return GPG_ERR_INV_ARG;

  match = ZxcvbnMatch (line+8, NULL, NULL);
  match = (match/MAX_ENTROPY)*100;
  snprintf (buf, sizeof (buf), "%u", (unsigned)match);
  return assuan_send_data (pwm->pctx, buf, strlen (buf));
}
#endif

static gpg_error_t
set_pinentry_strings (pwm_t * pwm, pwmd_pinentry_t which)
{
  char *tmp, *desc = NULL;
  gpg_error_t rc;

  if (which != PWMD_PINENTRY_USER)
    {
      rc = pinentry_command (pwm, NULL, NULL, "SETERROR ");
      if (rc)
	return rc;
    }

  tmp = pwmd_malloc (ASSUAN_LINELENGTH + 1);
  if (!tmp)
    return gpg_error_from_errno (ENOMEM);

  if (!pwm->pinentry_prompt)
    {
      pwm->pinentry_prompt = pwmd_strdup (N_("Passphrase:"));
      if (!pwm->pinentry_prompt)
	{
	  pwmd_free (tmp);
	  return GPG_ERR_ENOMEM;
	}
    }

  if (!pwm->pinentry_desc)
    {
      if (which == PWMD_PINENTRY_OPEN || which == PWMD_PINENTRY_OPEN_FAILED)
        {
          if (pwm->passphrase_hint && pwm->passphrase_info)
            {
              char *hint = pwmd_strdup (pwm->passphrase_hint), *hintp = hint;
              char *info = pwmd_strdup (pwm->passphrase_info), *infop;
              char *userid = strchr (hint, ' ');

              if (userid && *userid)
                *userid++ = 0;

              infop = strchr (info, ' ');
              if (infop && *infop)
                {
                  char *p = strchr (++infop, ' ');

                  if (p)
                    infop[strlen(infop)-strlen(p)] = 0;
                }

              hintp += 8;
              infop += 8;
              desc = pwmd_strdup_printf (N_
                                         ("Please enter the passphrase to unlock the OpenPGP secret key:%%0A%%0AClient: %s%%0AData file: %s%%0AKey ID: %s (0x%s)%%0AMain key ID: 0x%s"), pwm->name ? pwm->name : N_("unknown"), pwm->filename, userid, hintp, infop);
              pwmd_free (hint);
              pwmd_free (info);
            }
          else
            {
              desc = pwmd_strdup_printf (N_ ("A passphrase is required to decrypt the encrypted%%0Adata file \"%s\". Please enter the passphrase below."), pwm->filename);
            }
        }
      else if (which == PWMD_PINENTRY_SAVE
	       || which == PWMD_PINENTRY_SAVE_FAILED)
	desc =
	  pwmd_strdup_printf (N_
			      ("Please enter the passphrase to use to encrypt%%0Athe data file \"%s\"."),
			      pwm->filename);
      else if (which == PWMD_PINENTRY_SAVE_CONFIRM)
	desc =
	  pwmd_strdup_printf (N_
			      ("Please re-enter the passphrase for confirmation."),
			      pwm->filename);

      if (!desc)
	{
	  pwmd_free (tmp);
	  return GPG_ERR_ENOMEM;
	}
    }
  else
    desc = pwm->pinentry_desc;

  if (which == PWMD_PINENTRY_USER)
    {
      snprintf (tmp, ASSUAN_LINELENGTH, "SETERROR %s",
		pwm->pinentry_error ? pwm->pinentry_error : "");
      rc = pinentry_command (pwm, NULL, NULL, tmp);
      if (rc)
	{
	  pwmd_free (tmp);
	  return rc;
	}

      snprintf (tmp, ASSUAN_LINELENGTH, "SETDESC %s",
		pwm->pinentry_desc ? pwm->pinentry_desc : "");
    }
  else
    {
      if (which == PWMD_PINENTRY_SAVE_FAILED
	  || which == PWMD_PINENTRY_OPEN_FAILED)
	{
	  if (which == PWMD_PINENTRY_SAVE_FAILED)
	    snprintf (tmp, ASSUAN_LINELENGTH, "SETERROR %s",
		      N_("Passphrases do not match, try again."));
	  else
	    {
	      if (pwm->pinentry_tries)
		{
		  strcpy (tmp, "SETERROR ");
		  snprintf (tmp + strlen ("SETERROR "), ASSUAN_LINELENGTH,
			    N_("Bad passphrase (try %i of %i)"),
			    pwm->pinentry_try + 1, pwm->pinentry_tries);
		}
	      else
		snprintf (tmp, ASSUAN_LINELENGTH, "SETERROR %s",
			  N_("Bad passphrase, try again"));
	    }

	  rc = pinentry_command (pwm, NULL, NULL, tmp);
	  if (rc)
	    {
	      pwmd_free (tmp);
	      return rc;
	    }
	}

      snprintf (tmp, ASSUAN_LINELENGTH, "SETDESC %s", desc);

      if (pwm->pinentry_desc != desc)
	pwmd_free (desc);
    }

  rc = pinentry_command (pwm, NULL, NULL, tmp);
  if (rc)
    {
      pwmd_free (tmp);
      return rc;
    }

  snprintf (tmp, ASSUAN_LINELENGTH, "SETPROMPT %s", pwm->pinentry_prompt);
  rc = pinentry_command (pwm, NULL, NULL, tmp);
  pwmd_free (tmp);

#ifdef WITH_QUALITY
  if (!rc && (which == PWMD_PINENTRY_SAVE ||
	      which == PWMD_PINENTRY_SAVE_FAILED))
    {
      rc = pinentry_command (pwm, NULL, NULL, "SETQUALITYBAR");
      if (!rc)
	{
	  pwm->_inquire_func = quality_cb;
	  pwm->_inquire_data = pwm;
	}
    }
#endif

  if (!rc && pwm->pinentry_timeout >= 0)
    {
      char buf[32];

      snprintf(buf, sizeof(buf), "SETTIMEOUT %i", pwm->pinentry_timeout);
      rc = pinentry_command(pwm, NULL, NULL, buf);
    }

  return rc;
}

void
_pinentry_disconnect (pwm_t * pwm)
{
  if (pwm->pctx)
    assuan_release (pwm->pctx);

  pwm->pctx = NULL;
  pwm->pinentry_pid = -1;
}

gpg_error_t
_getpin (pwm_t * pwm, char **result, size_t * len, pwmd_pinentry_t which)
{
  gpg_error_t rc = set_pinentry_strings (pwm, which);

  if (rc)
    {
      _pinentry_disconnect (pwm);
      return rc;
    }

  rc = pinentry_command (pwm, result, len,
			 which == PWMD_PINENTRY_CONFIRM ? "CONFIRM" : "GETPIN");
  if (rc)
    {
      _pinentry_disconnect (pwm);

      /* This lets PWMD_OPTION_PINENTRY_TIMEOUT work. Note that it is not
       * thread safe do to the global variables. */
      if (gpg_err_code (rc) == GPG_ERR_EOF)
	rc = GPG_ERR_CANCELED;
    }
  else if (which != PWMD_PINENTRY_CONFIRM && len && result)
    {
      if (*len)
	*len = strlen (*result);	// remove the null byte
      else
	{
	  *result = pwmd_malloc (1);
	  *(*result) = 0;
	  *len = 1;
	}
    }

  return rc;
}

gpg_error_t
_pwmd_getpin (pwm_t * pwm, const char *filename, char **result,
	      size_t * len, pwmd_pinentry_t which)
{
  gpg_error_t rc;
  char *p;

  if (!pwm)
    return GPG_ERR_INV_ARG;

  p = pwm->filename;
  if (which == PWMD_PINENTRY_CLOSE)
    {
      _pinentry_disconnect (pwm);
      return 0;
    }

  if (!result && which != PWMD_PINENTRY_CONFIRM)
    return GPG_ERR_INV_ARG;

  pwm->filename = (char *) filename;
  rc = _getpin (pwm, result, len, which);
  pwm->filename = p;
  return rc;
}
