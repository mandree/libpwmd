/*
    Copyright (C) 2006-2021 Ben Kibbey <bjk@luxsci.net>

    This file is part of libpwmd.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#ifndef PINENTRY_H
#define PINENTRY_H

#include "types.h"

void _pinentry_disconnect (pwm_t * pwm);
gpg_error_t _getpin (pwm_t * pwm, char **result, size_t *,
		     pwmd_pinentry_t which);
gpg_error_t _do_save_getpin (pwm_t * pwm, char **password, size_t *);
gpg_error_t _pinentry_open (pwm_t * pwm, const char *filename,
			    char **password, size_t *);
gpg_error_t _pwmd_getpin (pwm_t * pwm, const char *filename, char **result,
			  size_t * len, pwmd_pinentry_t which);

#endif
