/*
    Copyright (C) 2012-2021 Ben Kibbey <bjk@luxsci.net>

    This file is part of libpwmd.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <ctype.h>
#include <sys/types.h>
#include <fcntl.h>

#ifdef HAVE_STRINGS_H
#include <strings.h>
#endif

#ifndef __MINGW32__
#include <sys/socket.h>
#endif

#include "types.h"
#include "misc.h"

#define DEFAULT_TLS_PRIORITY "SECURE256:SECURE192:SECURE128:" \
  "-VERS-SSL3.0:-VERS-TLS1.0"
#define WAIT_INTERVAL 50000
#define TEST_TIMEOUT(pwm, timeout, ret, start, rc)			\
  do {									\
    rc = 0;		      						\
    if (ret == GNUTLS_E_AGAIN || pwm->cancel)				\
      {									\
	time_t now = time (NULL);					\
	if (pwm->cancel || (timeout && now - start >= timeout))		\
	  {								\
            if (pwm->cancel)						\
              rc = gpg_error (GPG_ERR_CANCELED);			\
            else							\
              rc = gpg_error (GPG_ERR_ETIMEDOUT);			\
            ret = GNUTLS_E_TIMEDOUT;					\
	    break;							\
	  }								\
        struct timeval totv = { 0, WAIT_INTERVAL };			\
        select (0, NULL, NULL, NULL, &totv);				\
      }									\
    if (ret != GNUTLS_E_INTERRUPTED)					\
      break;								\
  }									\
  while (0)

void
tls_free (pwm_t *pwm)
{
  struct tls_s *tls = pwm->tcp->tls;
  int ret = 0;

  if (!tls)
    return;

  pwmd_free (tls->ca);
  pwmd_free (tls->cert);
  pwmd_free (tls->key);
  pwmd_free (tls->priority);
  pwmd_free (tls->server_fp);

  if (tls->session)
    {
      time_t start = time (NULL);

      do
	{
	  gpg_error_t rc = 0;

	  ret = gnutls_bye (tls->session, GNUTLS_SHUT_WR);
	  TEST_TIMEOUT (pwm, tls->timeout, ret, start, rc);
	  if (rc)
	    break;
	}
      while (ret == GNUTLS_E_AGAIN || ret == GNUTLS_E_INTERRUPTED);

      gnutls_deinit (tls->session);
    }
  else if (pwm->fd != ASSUAN_INVALID_FD)
    {
      __assuan_close (pwm->ctx, pwm->fd);
      pwm->fd = ASSUAN_INVALID_FD;
    }

  if (tls->x509)
    gnutls_certificate_free_credentials (tls->x509);

  tls->session = NULL;
  tls->x509 = NULL;
  pwmd_free (tls);
  pwm->tls_error = ret;
}

ssize_t
tls_read_hook (pwm_t *pwm, assuan_fd_t fd, void *data, size_t len)
{
  struct tls_s *tls = pwm->tcp->tls;
  ssize_t ret;
  time_t start = time (NULL);

  if (pwm->fd == ASSUAN_INVALID_FD)
    return -1;

  (void)fd;
  do
    {
      gpg_error_t rc = 0;
      struct timeval tv = { 0, WAIT_INTERVAL };
      fd_set fds;
      int n;

      FD_ZERO (&fds);
      FD_SET (HANDLE2SOCKET (pwm->fd), &fds);
      n = select (HANDLE2SOCKET (pwm->fd)+1, &fds, NULL, NULL, &tv);
      if (n == 0 && tls->nl)
        {
          char c[] = "#\n";

          memcpy (data, c, sizeof(c));
          ret = strlen (c);
          break;
        }

      ret = gnutls_record_recv (tls->session, data, len);
      if (ret == GNUTLS_E_REHANDSHAKE)
	{
	  do
	    {
	      ret = gnutls_handshake (tls->session);
	      TEST_TIMEOUT (pwm, tls->timeout, ret, start, rc);
	      if (rc)
                break;
	    }
	  while (ret < 0 && gnutls_error_is_fatal (ret) == 0);

          if (!ret && tls->nl)
            {
              char c[] = "#\n";

              memcpy (data, c, sizeof(c));
              ret = strlen (c);
              break;
            }
          else if (ret)
            break;
          else
            ret = GNUTLS_E_AGAIN;
	}

      TEST_TIMEOUT (pwm, tls->timeout, ret, start, rc);
      if (rc)
        break;
    }
  while (ret == GNUTLS_E_AGAIN || ret == GNUTLS_E_INTERRUPTED);

  if (ret < 0)
    pwm->tls_error = ret;

  if (!ret)
    return 0;
  else if (ret == GNUTLS_E_PREMATURE_TERMINATION)
    errno = EPIPE;

  if (ret > 0)
    tls->nl = ((char *)data)[ret-1] == '\n';

  return ret < 0 ? -1 : ret;
}

ssize_t
tls_write_hook (pwm_t *pwm, assuan_fd_t fd, const void *data, size_t len)
{
  struct tls_s *tls = pwm->tcp->tls;

  /* This is probably the BYE command after a previous call timed
   * out. If not done, the time(2) call seems to hang.*/
  if (pwm->fd == ASSUAN_INVALID_FD)
    return -1;

  ssize_t ret;
  time_t start = time (NULL);

  (void)fd;
  do
    {
      gpg_error_t rc = 0;

      ret = gnutls_record_send (tls->session, data, len);
      TEST_TIMEOUT (pwm, tls->timeout, ret, start, rc);
      if (rc)
        break;
    }
  while (ret == GNUTLS_E_AGAIN || ret == GNUTLS_E_INTERRUPTED);

  if (ret < 0)
    pwm->tls_error = ret;

  if (!ret)
    return 0;
  else if (ret == GNUTLS_E_PREMATURE_TERMINATION)
    errno = EPIPE;

  return ret < 0 ? -1 : ret;
}

static gpg_error_t
tls_fingerprint (pwm_t *pwm, char **result)
{
  gnutls_session_t ses = pwm->tcp->tls->session;
  gnutls_x509_crt_t crt;
  const gnutls_datum_t *cert_list;
  unsigned count;
  unsigned char buf[32];
  size_t len = sizeof (buf);

  *result = NULL;
  gnutls_x509_crt_init (&crt);
  if (!crt)
    return GPG_ERR_ENOMEM;

  cert_list = gnutls_certificate_get_peers (ses, &count);
  if (count)
    {
      pwm->tls_error = gnutls_x509_crt_import (crt, &cert_list[0],
                                               GNUTLS_X509_FMT_DER);
      if (!pwm->tls_error)
        gnutls_x509_crt_get_fingerprint (crt, GNUTLS_DIG_SHA256, buf, &len);
    }

  gnutls_x509_crt_deinit (crt);
  if (!count)
    {
      pwm->tls_error = GNUTLS_CERT_INVALID;
      return GPG_ERR_MISSING_CERT;
    }

  if (!pwm->tls_error)
    *result = bin2hex (buf, len);

  return !pwm->tls_error ? 0 : GPG_ERR_ENOMEM;
}

static gpg_error_t
verify_server_certificate (unsigned status)
{
  if (status & GNUTLS_CERT_REVOKED)
    {
      fprintf (stderr, "server certificate is revoked\n");
      return GPG_ERR_CERT_REVOKED;
    }

  if (status & GNUTLS_CERT_SIGNER_NOT_FOUND)
    {
      fprintf (stderr, "server certificate has no signer\n");
      return GPG_ERR_NO_SIGNATURE_SCHEME;
    }

  if (status & GNUTLS_CERT_SIGNATURE_FAILURE)
    {
      fprintf (stderr, "server certificate signature verification failed\n");
      return GPG_ERR_BAD_SIGNATURE;
    }

  if (status & GNUTLS_CERT_EXPIRED)
    {
      fprintf (stderr, "server certificate expired\n");
      return GPG_ERR_CERT_EXPIRED;
    }

  if (status & GNUTLS_CERT_SIGNER_NOT_CA)
    {
      fprintf (stderr, "server certificate signer is not from CA\n");
      return GPG_ERR_BAD_CA_CERT;
    }

  if (status & GNUTLS_CERT_INSECURE_ALGORITHM)
    {
      fprintf (stderr, "server certificate has insecure algorithm\n");
      return GPG_ERR_UNSUPPORTED_ALGORITHM;
    }

  if (status)
    {
      fprintf (stderr, "server certificate is invalid: %u\n", status);
      return GPG_ERR_BAD_CERT;
    }

  return 0;
}

static gpg_error_t
verify_certificate (pwm_t *pwm)
{
  unsigned int status;
  const gnutls_datum_t *cert_list;
  unsigned int cert_list_size;
  int i;
  gnutls_x509_crt_t cert;
  gpg_error_t rc = 0;

  i = gnutls_certificate_verify_peers2 (pwm->tcp->tls->session, &status);
  if (i < 0)
    {
      pwm->tls_error = i;
      return GPG_ERR_BAD_CERT;
    }

  rc = verify_server_certificate (status);
  if (rc)
    return rc;

  if (gnutls_certificate_type_get (pwm->tcp->tls->session) != GNUTLS_CRT_X509)
    return GPG_ERR_UNSUPPORTED_CERT;

  if (gnutls_x509_crt_init (&cert) < 0)
    return gpg_error_from_errno (ENOMEM);

  cert_list = gnutls_certificate_get_peers (pwm->tcp->tls->session,
					    &cert_list_size);
  if (!cert_list)
    {
      rc = GPG_ERR_MISSING_CERT;
      goto done;
    }

  for (i = 0; i < cert_list_size; i++)
    {
      pwm->tls_error = gnutls_x509_crt_import (cert, &cert_list[i],
                                               GNUTLS_X509_FMT_DER);
      if (pwm->tls_error < 0)
	{
	  rc = GPG_ERR_BAD_CERT_CHAIN;
	  goto done;
	}

      if (gnutls_x509_crt_get_expiration_time (cert) < time (0))
	{
	  rc = GPG_ERR_CERT_EXPIRED;
	  goto done;
	}

      if (gnutls_x509_crt_get_activation_time (cert) > time (0))
	{
	  rc = GPG_ERR_CERT_TOO_YOUNG;
	  goto done;
	}

      if (pwm->tcp->tls->verify)
	{
	  if (!gnutls_x509_crt_check_hostname (cert, pwm->tcp->host))
	    {
	      rc = GPG_ERR_BAD_CERT_CHAIN;
	      goto done;
	    }
	}
    }

  if (pwm->tcp->tls->server_fp)
    {
      char *result;

      rc = tls_fingerprint (pwm, &result);
      if (!rc)
	{
	  if (!result || !*result ||
	      strcasecmp (result, pwm->tcp->tls->server_fp))
	    rc = GPG_ERR_BAD_CERT;

	  pwmd_free (result);
	}
    }

done:
  gnutls_x509_crt_deinit (cert);
  return rc;
}

static gpg_error_t
tls_init (pwm_t * pwm)
{
  gpg_error_t rc;
  int ret;
  const char *errstr;

  ret = gnutls_certificate_allocate_credentials (&pwm->tcp->tls->x509);
  if (ret)
    {
      pwm->tls_error = ret;
      return gpg_error_from_errno (ENOMEM);
    }

  /* The client certificate must be signed by the CA of the pwmd server
   * certificate in order for the client to authenticate successfully. Man in
   * the middle attacks are still possible if the attacker is running a pwmd
   * that doesn't require client certificate authentication. So require the
   * client to verify the server certificate.
   */
  ret = gnutls_certificate_set_x509_trust_file (pwm->tcp->tls->x509,
						pwm->tcp->tls->ca,
						GNUTLS_X509_FMT_PEM);
  if (ret == -1)
    {
      rc = GPG_ERR_INV_CERT_OBJ;
      goto fail;
    }

  ret = gnutls_certificate_set_x509_key_file (pwm->tcp->tls->x509,
					      pwm->tcp->tls->cert,
					      pwm->tcp->tls->key,
					      GNUTLS_X509_FMT_PEM);
  if (ret != GNUTLS_E_SUCCESS)
    {
      pwm->tls_error = ret;
      rc = GPG_ERR_INV_CERT_OBJ;
      goto fail;
    }

  ret = gnutls_init (&pwm->tcp->tls->session, GNUTLS_CLIENT);
  if (ret != GNUTLS_E_SUCCESS)
    {
      pwm->tls_error = ret;
      rc = GPG_ERR_INV_CERT_OBJ;
      goto fail;
    }

  ret = gnutls_priority_set_direct (pwm->tcp->tls->session,
				    pwm->tcp->tls->priority
                                    ? pwm->tcp->tls->priority
                                    : DEFAULT_TLS_PRIORITY,
                                    &errstr);
  if (ret != GNUTLS_E_SUCCESS)
    {
      pwm->tls_error = ret;
      rc = GPG_ERR_INV_CERT_OBJ;
      goto fail;
    }

  ret = gnutls_credentials_set (pwm->tcp->tls->session, GNUTLS_CRD_CERTIFICATE,
			       pwm->tcp->tls->x509);
  if (ret != GNUTLS_E_SUCCESS)
    {
      pwm->tls_error = ret;
      rc = GPG_ERR_INV_CERT_OBJ;
      goto fail;
    }

  gnutls_transport_set_ptr (pwm->tcp->tls->session,
			    (gnutls_transport_ptr_t) HANDLE2SOCKET (pwm->fd));
  rc = set_non_blocking (pwm->fd, 1);
  if (rc)
    goto fail;

  time_t start = time (NULL);
  do
    {
      ret = gnutls_handshake (pwm->tcp->tls->session);
      TEST_TIMEOUT (pwm, pwm->tcp->tls->timeout, ret, start, rc);
      if (rc)
	goto fail;
    }
  while (ret == GNUTLS_E_AGAIN || ret == GNUTLS_E_INTERRUPTED);

  if (ret != GNUTLS_E_SUCCESS)
    {
      pwm->tls_error = ret;
      rc = GPG_ERR_INV_CERT_OBJ;
      goto fail;
    }

  rc = verify_certificate (pwm);

fail:
  if (rc)
    {
      /* Keep the original error since it maybe overwritten during TLS
       * shutdown. */
      ret = pwm->tls_error;
      /* The session may not have completed a handshake. Will crash during
       * gnutls_bye(). */
      gnutls_deinit (pwm->tcp->tls->session);
      pwm->tcp->tls->session = NULL;
      free_tcp (pwm);
      pwm->tls_error = ret;
    }

  return rc;
}

gpg_error_t
tls_connect (pwm_t * pwm, const char *host, int port, const char *cert,
             const char *key, const char *cacert, const char *prio,
             const char *server_fp, int verify)
{
  struct tcp_s *tcp;
  gpg_error_t rc;

  if (!cert || !key || !cacert)
    return GPG_ERR_INV_ARG;

  tcp = pwmd_calloc (1, sizeof (struct tcp_s));
  if (!tcp)
    return GPG_ERR_ENOMEM;

  pthread_cond_init (&tcp->dns_cond, NULL);
  pthread_mutex_init (&tcp->dns_mutex, NULL);
  pwm->tcp = tcp;
  tcp->port = port;
  tcp->host = pwmd_strdup (host);
  if (!tcp->host)
    {
      rc = GPG_ERR_ENOMEM;
      goto fail;
    }

  tcp->tls = pwmd_calloc (1, sizeof (struct tls_s));
  if (!tcp->tls)
    {
      rc = GPG_ERR_ENOMEM;
      goto fail;
    }

  tcp->tls->timeout = pwm->socket_timeout;
  tcp->tls->verify = verify;
  tcp->tls->cert = pwmd_strdup (cert);
  if (!tcp->tls->cert)
    {
      rc = GPG_ERR_ENOMEM;
      goto fail;
    }

  tcp->tls->key = pwmd_strdup (key);
  if (!tcp->tls->key)
    {
      rc = GPG_ERR_ENOMEM;
      goto fail;
    }

  tcp->tls->ca = pwmd_strdup (cacert);
  if (!tcp->tls->ca)
    {
      rc = GPG_ERR_ENOMEM;
      goto fail;
    }

  if (prio)
    {
      tcp->tls->priority = pwmd_strdup (prio);
      if (!tcp->tls->priority)
	{
	  rc = GPG_ERR_ENOMEM;
	  goto fail;
	}
    }

  if (server_fp)
    {
      tcp->tls->server_fp = pwmd_strdup (server_fp);
      if (!tcp->tls->server_fp)
	{
	  rc = GPG_ERR_ENOMEM;
	  goto fail;
	}
    }

  rc = tcp_connect_common (pwm);
  if (!rc)
    {
      rc = tls_init (pwm);
      if (!rc)
        {
#ifdef __MINGW32__
	  rc = assuan_socket_connect_fd (pwm->ctx, pwm->fh, 0);
#else
	  rc = assuan_socket_connect_fd (pwm->ctx, pwm->fd, 0);
#endif
	}
    }

fail:
  if (rc)
    free_tcp (pwm);

  return rc;
}

/*
 * tls[46]://[hostname][:port]
 */
gpg_error_t
tls_parse_url (const char *str, char **host, int *port)
{
  *port = 6466;
  return parse_hostname_common (str, host, port);
}
