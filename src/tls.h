/*
    Copyright (C) 2012-2021 Ben Kibbey <bjk@luxsci.net>

    This file is part of libpwmd.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#ifndef TLS_H
#define TLS_H

#ifdef WITH_GNUTLS
#include <gnutls/gnutls.h>
#include <gnutls/x509.h>
#endif

struct tls_s
{
  char *cert;
  char *key;
  char *ca;
  int verify;
  char *priority;
  char *server_fp;
  gnutls_session_t session;
  gnutls_certificate_credentials_t x509;
  int timeout;
  int nl;
};

ssize_t tls_write_hook (pwm_t *, assuan_fd_t fd, const void *data, size_t len);
ssize_t tls_read_hook (pwm_t *, assuan_fd_t fd, void *data, size_t len);
void tls_free (pwm_t *);
gpg_error_t tls_parse_url (const char *str, char **host, int *port);
gpg_error_t tls_connect (pwm_t * pwm, const char *host, int port,
                         const char *cert, const char *key,
                         const char *cacert, const char *prio,
                         const char *server_fp, int verify);

#endif
